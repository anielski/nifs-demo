-module (hello_nif).
-export ([square/1]).
-on_load(init/0).

-define (LIBNAME, "hello_nif").

init() ->
	SOName = filename:join([priv, ?LIBNAME]),
	ok = erlang:load_nif(SOName, 0).

square(_X) ->
	exit(nif_library_not_loaded).