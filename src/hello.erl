-module (hello).
-export ([test/0, hello/0, hello_labs/0, hello_res/0]).


test() ->
    hello_res()
    % , hello()
    % ,hello_labs()
    .

hello() ->
    % L = [float(T) || T <- lists:seq(1,10)],
    % L = [-35.78917822987577,-29.055144381290376,19.714078430055196,-34.018857993684406,5.825580837529017,-28.502695176467864,-4.20759702258016,-7.86012397862352,-49.40773273739643,6.21197686697257],
    L = [0.0000000000,1.0000000000,2.0000000000,3.0000000000,4.0000000000,5.0000000000,6.0000000000,7.0000000000,8.0000000000,9.0000000000],   % hello_nif:square(3).
    % io:format("~p~n",[L]),
    io:format("Square of ~p: ~p~n",[L,lists:map(fun hello_nif:square/1, L)]),
    io:format("Sum of ~p: ~p~n",[L,fitness_nif:fitness(L)]),
    ok.

hello_res() ->
    
    {MutationRange, MutationRate} = {0.05, 0.1},

    BinSol10 = fitness_nif:create_solution(10),
    BinSol20 = fitness_nif:create_solution(20),
    % io:format("BinSol10: ~p~n",[BinSol10]), 
    % io:format("BinSol20: ~p~n",[BinSol20]), 
    Fit10 = fitness_nif:evaluate_solution(BinSol10),
    Fit20 = fitness_nif:evaluate_solution(BinSol20),
    io:format("Fit10: ~p~n",[Fit10]), 
    io:format("Fit20: ~p~n",[Fit20]), 
    
    MutBinSol10 = fitness_nif:mutate_solution(BinSol10, MutationRange, MutationRate),
    MutBinSol20 = fitness_nif:mutate_solution(BinSol20, MutationRange, MutationRate),
    MutFit10 = fitness_nif:evaluate_solution(MutBinSol10),
    MutFit20 = fitness_nif:evaluate_solution(MutBinSol20),
    io:format("MutFit10: ~p~n",[MutFit10]), 
    io:format("MutFit20: ~p~n",[MutFit20]), 

    BinSol10_2 = fitness_nif:create_solution(10),
    Fit10_2 = fitness_nif:evaluate_solution(BinSol10_2),
    io:format("Fit10_2: ~p~n",[Fit10_2]), 
    {Recombined1, Recombined2} = fitness_nif:recombine_solutions(BinSol10, BinSol10_2),
    RecFit1 = fitness_nif:evaluate_solution(Recombined1),
    RecFit2 = fitness_nif:evaluate_solution(Recombined2),
    io:format("RecFit1: ~p~n",[RecFit1]),
    io:format("RecFit2: ~p~n",[RecFit2]),
    ok.

hello_labs() ->
    L = [(T rem 2)*2 - 1 || T <- lists:seq(1,10)],
    % L = [-1, 1, -1, 1, -1, -1, 1, -1, -1, 1, -1, 1, -1, 1, 1, 1, 1, 1, -1, 1, 1, -1, -1, 1, -1, -1, -1, 1, -1, -1, -1, -1, 1, 1, 1, -1, 1, 1, -1, 1, 1, -1, -1, -1, 1, 1, 1, -1, 1, 1, -1, 1, -1, -1, -1, 1, -1, -1, -1, 1, 1, -1, -1, -1, 1, -1, 1, -1, -1, -1, -1, -1, -1, 1, 1, 1, -1, -1, -1, -1, -1],
    io:format("Labs(~p) = ~p~n",[L,fitness_nif:fitness_labs(L)]),
    ok.
