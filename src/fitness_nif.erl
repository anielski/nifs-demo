-module (fitness_nif).
-export ([fitness/1, fitness_labs/1, create_solution/1, 
	evaluate_solution/1, mutate_solution/3, recombine_solutions/2]).
-on_load(init/0).

-define (LIBNAME, "fitness_nif").

init() ->
    SOName = filename:join([priv, ?LIBNAME]) , 
    ok = erlang:load_nif(SOName, 0).

fitness(_Solution) ->
    exit(nif_not_loaded).

fitness_labs(_Solution) ->
    exit(nif_not_loaded).

create_solution(_ProblemSize) ->
    exit(nif_not_loaded).

evaluate_solution(_Solution) ->
    exit(nif_not_loaded).

mutate_solution(_Solution, _Range, _Rate) ->
    exit(nif_not_loaded).

recombine_solutions(_Solution1, _Solution2) ->
    exit(nif_not_loaded).