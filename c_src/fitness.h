#include "math.h"

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

#define M_TWOPI (2.*M_PI)

double rastrigin(double* x, int len);
double f_labs(short* x, short n);