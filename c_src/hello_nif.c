#include "erl_nif.h"

extern double square(double x);

static ERL_NIF_TERM square_nif(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[]){

	double x, ret;
	if (!enif_get_double(env, argv[0], &x)){
		return enif_make_badarg(env);
	}
	ret = square(x);
	return enif_make_double(env, ret);
}

static ErlNifFunc nif_funcs[] = {
	{"square",1,square_nif}
};

ERL_NIF_INIT(hello_nif, nif_funcs, NULL, NULL, NULL, NULL)