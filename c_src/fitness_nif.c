#include "erl_nif.h"
#include "stdio.h"
#include "stdlib.h"
#include "time.h"
#include "fitness.h"


// extern double rastrigin(double* x, int len);
// extern double f_labs(short* x, short n);


#define SEED 0
#define DEBUG


typedef struct {
    int len;
    double* genotype;
} Solution;

double randdouble(double lower, double upper){
    return lower + (upper - lower) * ((double) rand()) / RAND_MAX;
}

void print_solution(Solution* sol, const char* desc){
    int i;
    printf("%s:",desc);
    for(i=0; i<sol->len; i++){
        printf("%.10f ", sol->genotype[i]);
    }
    printf("\n");
}

#define CHECK(env, obj) if (!(obj)){return enif_make_badarg((env));}


static void solution_dtor(ErlNifEnv* env, void* obj){
    Solution* sol = (Solution*) obj;
    if (sol != NULL && sol->genotype != NULL){
        free(sol->genotype);
    }
}

static int nif_load(ErlNifEnv* env, void** priv, ERL_NIF_TERM load_info){
    const char* mod = "rastrigin_ops_nif";
    const char* name = "Solution";
    int flags = ERL_NIF_RT_CREATE; // | ERL_NIF_RT_TAKEOVER;

    *priv = enif_open_resource_type(env, mod, name, solution_dtor, flags, NULL);

#ifdef SEED
    srand(SEED);
#else 
    srand(time(NULL));
#endif

    return 0;
}

static ERL_NIF_TERM evaluate_solution(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[]){

    ErlNifResourceType* sol_type;
    Solution* sol;
    double fitness;

    CHECK(env, argc == 1)

    sol_type = (ErlNifResourceType*) enif_priv_data(env);
    CHECK(env, sol_type)

    CHECK(env, enif_get_resource(env, argv[0], sol_type, (void**) &sol))

#ifdef DEBUG
    print_solution(sol,"Genotype");
#endif

    fitness = rastrigin(sol->genotype, sol->len);

    return enif_make_double(env, fitness);

}

static ERL_NIF_TERM recombine_solutions(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[]){

    ERL_NIF_TERM terms[2];
    ErlNifResourceType* sol_type;
    Solution *sols[2], *new_sols[2];
    unsigned int i;
    unsigned int len;

    CHECK(env, argc == 2)

    sol_type = (ErlNifResourceType*) enif_priv_data(env);
    CHECK(env, sol_type)

    // read parent solutions
    for (i=0; i<2; i++){
        CHECK(env, enif_get_resource(env, argv[i], sol_type, (void**) &sols[i]))
    }

#ifdef DEBUG
    print_solution(sols[0],"Genotype1");
    print_solution(sols[1],"Genotype2");
#endif
    
    // allocate 2 child solution structures
    len = sols[0]->len;
    for (i=0; i<2; i++){
        new_sols[i] = (Solution*) enif_alloc_resource(sol_type, sizeof(Solution));
        CHECK(env, new_sols[i])
        
        terms[i] = enif_make_resource(env, new_sols[i]);
        CHECK(env,terms[i])
        enif_release_resource(new_sols[i]);

        new_sols[i]->len = len;
        new_sols[i]->genotype = (double*) malloc(sizeof(double)*len);
    }

    // recombine parent solutions into 2 child solutions
    for (i=0;i<len;i++){
        double lower, upper;
        if (sols[0]->genotype[i] >= sols[1]->genotype[i]){
            lower = sols[1]->genotype[i];
            upper = sols[0]->genotype[i];
        } else {
            lower = sols[0]->genotype[i];
            upper = sols[1]->genotype[i];
        }
        new_sols[0]->genotype[i] = randdouble(lower,upper);
        new_sols[1]->genotype[i] = randdouble(lower,upper);
    }

#ifdef DEBUG
    print_solution(new_sols[0],"RecombinedGenotype1");
    print_solution(new_sols[1],"RecombinedGenotype2");
#endif
    
    return enif_make_tuple2(env, terms[0], terms[1]);
}


double mutate_feature(double feature, double mutation_range){
    double range;
    range = randdouble(0.0, mutation_range);
    if (range < 0.2){
        range = 5.0;
    } else if (range < 0.4){
        range = 0.2;
    } else {
        range = 1.0;
    }
    return feature + range * tan(M_PI * randdouble(-0.5, 0.5));
}


static ERL_NIF_TERM mutate_solution(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[]){

    ERL_NIF_TERM term;
    ErlNifResourceType* sol_type;
    Solution *sol, *mut_sol;
    unsigned int i;
    double range, rate;

    CHECK(env, argc == 3)

    sol_type = (ErlNifResourceType*) enif_priv_data(env);
    CHECK(env, sol_type)

    CHECK(env, enif_get_resource(env, argv[0], sol_type, (void**) &sol))
    CHECK(env, enif_get_double(env, argv[1], &range))
    CHECK(env, enif_get_double(env, argv[2], &rate))

    mut_sol = (Solution*) enif_alloc_resource(sol_type, sizeof(Solution));
    CHECK(env, mut_sol)
    
    term = enif_make_resource(env, mut_sol);
    CHECK(env,term)
    enif_release_resource(mut_sol);

    mut_sol->len = sol->len;
    mut_sol->genotype = (double*) malloc(sizeof(double)*sol->len);

    for (i=0;i<sol->len;i++){
        if (randdouble(0.0,1.0) < rate){
            mut_sol->genotype[i] = mutate_feature(sol->genotype[i], range);
        } else {
            mut_sol->genotype[i] = sol->genotype[i];
        }
    }

#ifdef DEBUG
    print_solution(sol, "Genotype");
    print_solution(mut_sol, "MutatedGenotype");
#endif

    return term;

}

static ERL_NIF_TERM create_solution(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[]){

    ERL_NIF_TERM term;
    ErlNifResourceType* sol_type;
    Solution* sol;
    unsigned int len;
    unsigned int i;

    CHECK(env, argc == 1)
    CHECK(env, enif_get_uint(env, argv[0], &len))

    sol_type = (ErlNifResourceType*) enif_priv_data(env);
    CHECK(env, sol_type)

    sol = (Solution*) enif_alloc_resource(sol_type, sizeof(Solution));
    CHECK(env, sol)
    
    term = enif_make_resource(env, sol);
    CHECK(env,term)
    enif_release_resource(sol);

    sol->len = len;
    sol->genotype = (double*) malloc(sizeof(double)*len);
    for (i=0;i<len;i++){
        sol->genotype[i] = randdouble(-50.0, 50.0);
    }

    return term;

}

void deb(double x[], int n);
void deblabs(short x[], int n);

static ERL_NIF_TERM rastrigin_nif(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[]){

    unsigned int len, idx = 0;
    ERL_NIF_TERM rest = argv[0],
        head, tail;


    if (!enif_get_list_length(env, rest, &len)){
        return enif_make_badarg(env);
    }
    
    double x[len], ret;

    while (enif_get_list_cell(env, rest, &head, &tail)) {
        if (!enif_get_double(env, head, &x[idx])) {
            return enif_make_badarg(env);
        }
        idx++;
        rest = tail;
    }

    deb(x,len);
    ret = rastrigin(x, len);
    return enif_make_double(env, ret);
}

static ERL_NIF_TERM labs_nif(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[]){

    unsigned int len, idx = 0;
    ERL_NIF_TERM rest = argv[0],
        head, tail;


    if (!enif_get_list_length(env, rest, &len)){
        return enif_make_badarg(env);
    }
    
    short x[len];
    double ret;
    int tmp;

    while (enif_get_list_cell(env, rest, &head, &tail)) {
        if (!enif_get_int(env, head, &tmp)) {
            return enif_make_badarg(env);
        }
        x[idx] = tmp;
        idx++;
        rest = tail;
    }

    deblabs(x,len);
    ret = f_labs(x, len);
    printf("ret:%f\n",ret);
    return enif_make_double(env, ret);
}

static ErlNifFunc nif_funcs[] = {
    {"fitness", 1, rastrigin_nif},
    {"fitness_labs", 1, labs_nif},
    {"create_solution", 1, create_solution},
    {"evaluate_solution", 1, evaluate_solution},
    {"mutate_solution", 3, mutate_solution},
    {"recombine_solutions", 2, recombine_solutions}
};

void deb(double x[], int n){
    printf("Got: ");
    int i = 0;
    for (i=0;i<n;i++){
        printf("%.16f ", x[i]);
    }
    printf("\n");
}

void deblabs(short x[], int n){
    printf("Got: ");
    int i = 0;
    for (i=0;i<n;i++){
        printf("%d ", x[i]);
    }
    printf("\n");
}

ERL_NIF_INIT(fitness_nif, nif_funcs, nif_load, NULL, NULL, NULL)