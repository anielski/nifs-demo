#include "fitness.h"

double rastrigin(double* x, int n){
    double s = 0.0;
    int i;

    for (i=0; i<n; i++){
        s += x[i]*x[i] - 10.0*cos(M_TWOPI*x[i]) + 10.0;
    }

    return s;
}

double f_labs(short* x, short n){
    double e = 0.0, m = 0.0, c = 0.0;
    int k,i;

    for (k=1; k<n; k++){
        c = 0.0;
        for (i=0; i<n-k; i++){
            c += x[i]*x[i+k];
        }
        e += c*c;
    }

    m = n*n*0.5/e;

    return m;
}